
struct oncoll {
    void (*hit)(struct oncoll *, struct oncoll *);
    void *data;
    struct oncoll *next;
};

void
oncolldo(struct oncoll *a, struct oncoll *b);

inline void
oncolladd(struct oncoll *a, struct oncoll *b);

struct meshcoll {
    float *mesh;
    int (*poll)(const float *, const float *);
    struct meshcoll *next;
};

inline void
meshcolladd(struct meshcoll *a, struct meshcoll *b);

struct coll {
    union {
        struct {
            float minx, miny, minz;
            float maxx, maxy, maxz;
        };
        float arr[6];
    }aabb;

    struct meshcoll *meshcoll;

    struct oncoll *onhit;
    struct oncoll *onray;
};

void
collpoll(struct coll *arr, int len);
