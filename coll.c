#include "coll.h"
#include <stdio.h>

void
oncolldo(struct oncoll *a, struct oncoll *b)
{
    while (a && b) {
        if (a->hit == b->hit) {
            a->hit(a, b);
            b->hit(b, a);
        }
        a = a->next;
        b = b->next;
    }
}

inline void
oncolladd(struct oncoll *a, struct oncoll *b)
{
    while (a->next) a = a->next;
    a->next = b;
}

inline void
meshcolladd(struct meshcoll *a, struct meshcoll *b)
{
    while (a->next) a = a->next;
    a->next = b;
}

static inline int
meshcollpoll(struct meshcoll *a, struct meshcoll *b)
{
    int ret = 0;
    while (a && b) {
        if (a->poll == b->poll) {
            ret = a->poll(a->mesh, b->mesh);
            if (!ret) return ret;
        } else break;

        a = a->next;
        b = b->next;
    }
    return ret;
}

void
collpoll(struct coll *arr, int c)
{
    struct coll *a, *b;
    int i, o;
    for (o = 0; o < c; ++o) {
        a = arr + o;
        for (i = o + 1; i < c; ++i) {
            b = arr + i;
            if (a->aabb.minx > b->aabb.maxx) continue;
            if (a->aabb.maxx < b->aabb.minx) continue;
            if (a->aabb.miny > b->aabb.maxy) continue;
            if (a->aabb.maxy < b->aabb.miny) continue;
            if (a->aabb.minz > b->aabb.maxz) continue;
            if (a->aabb.maxz < b->aabb.minz) continue;

            if (a->meshcoll && b->meshcoll) {
                if (!meshcollpoll(a->meshcoll, b->meshcoll)) {
                    continue;
                }
            }

            printf("Object %d collided with object %d!\n", o, i);
            oncolldo(a->onhit, b->onhit);
        }
    }
}
