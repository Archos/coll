#include <stdio.h>
#include <stdlib.h>
#include "coll.h"

int main()
{
    struct coll arr[] = {
        { 0, 0, 0, 2, 2, 2 },
        { 1, 1, 1, 4, 4, 4 },
        { 3, 3, 3, 5, 5, 5 },
    };
    int len = sizeof(arr) / sizeof(struct coll);
    collpoll(arr, len);
    return 0;
}
